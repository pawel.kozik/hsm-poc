# HSM

This PoC shows how to implement details from [RFC 0005 Issuer specific authentication](https://gitlab.com/kevineu/engineering/rfcs/-/blob/issuer-specific-authentication/rfc/rails/rails-core/0005-issuer-specific-authentication.md):

_Preparation_:

1. Generate a symmetric master key using [`genSymKey`][genSymKey]. Use 256bit AES, mark the key as non-extractable.

Note that for added security we might generate a separate master key for each Issuer. This would require replication
process to be initiated to other HSM clusters being used, such as in other regions.

Another alternative is to use `C_DeriveKey` (which is [supported][pkcs11apis] by PKCS #11 SDK) to generate one for
each Issuer from the main master key. While this might be done during the signing process, it might as well be
done once per-Issuer per-HSM-cluster with the key itself persisted inside the HSM for performance reasons.

The specific approach should be chosen after the initial performance testing and additional security considerations
have taken place.

_Key generation_:

1. Use [`genECCKeyPair`][genECCKeyPair] to generate ECC key pair inside the current session.
2. Use [`wrapKey`][wrapKey] to export the private key wrapped (encrypted) using the master key.
3. Store the wrapped private key in the database.
4. Use [`exportPubKey`][exportPubKey] to export the public key in plain text.
5. Pass the public key to the Issuer.
6. Close down the session, effectively destroying the key pair in the HSM storage.

_Generating the signature_:

1. Fetch the wrapped private key from the database.
2. Use [`unwrapKey`][unwrapKey] with the master key to load the key into the current session.
3. Instead of hashing inside the HSM, hash the contents to be signed locally (`SHA-256` with 256-bit keys).
4. Use [`sign`][sign] to generate the signature with [`CKM_ECDSA`][CKM_ECDSA].
5. Close down the session, effectively destroying the key pair in the HSM storage.

This approach allows to:

- Store and distribute the key materials separately and without too much security risks.
- Allow different HSM clusters to be used for each key.
- Ensure that there's no technical way, even with all the root accesses, to get the hang of the master or private keys.

[genSymKey]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/key_mgmt_util-genSymKey.html
[genECCKeyPair]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/key_mgmt_util-genECCKeyPair.html
[wrapKey]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/key_mgmt_util-wrapKey.html
[exportPubKey]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/key_mgmt_util-exportPubKey.html
[unwrapKey]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/key_mgmt_util-unwrapKey.html
[sign]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/key_mgmt_util-sign.html
[pkcs11apis]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/pkcs11-apis.html
[CKM_ECDSA]: https://docs.aws.amazon.com/cloudhsm/latest/userguide/pkcs11-mechanisms.html

## Install SOFTHSM2

```shell
$ ./configure --enable-ecc --with-objectstore-backend-db \
        --with-openssl=/opt/homebrew/opt/openssl \
        --with-sqlite3=/opt/homebrew/opt/sqlite

$ make
$ sudo make install
```

## Generate token

```shell
$ softhsm2-util --init-token --slot 1 --label "MyToken" --pin 1234 --so-pin 1234 --id 1
The token has been initialized and is reassigned to slot 2006428085
```
