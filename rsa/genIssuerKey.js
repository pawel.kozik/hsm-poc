const graphene = require("graphene-pk11");
const { publicKeyToPem } = require("pem-jwk");
const nodeRsa = require("node-rsa");

const softHsm = graphene.Module.load("/usr/local/lib/softhsm/libsofthsm2.so");
softHsm.initialize();

try {
  const slot = softHsm.getSlots(0, true);
  const session = slot.open(graphene.SessionFlag.SERIAL_SESSION);
  session.login("1234");

  const objects = session.find({
    class: graphene.ObjectClass.SECRET_KEY,
    keyType: graphene.KeyType.AES,
    label: "MasterKey3",
  });
  if (!objects.length) {
    throw new Error("Cannot get master key");
  }
  const masterKey = objects.items(0);

  const keyPair = session.generateKeyPair(
    graphene.KeyGenMechanism.RSA,
    {
      keyType: graphene.KeyType.RSA,
      modulusBits: 1024,
      publicExponent: Buffer.from([3]),
      token: false,
      verify: true,
      encrypt: true,
    },
    {
      keyType: graphene.KeyType.RSA,
      token: false,
      sign: true,
      decrypt: true,
      //wrap: true,
      unwrap: true,
      extractable: true,
    }
  );
  console.log("KeyPair generated");
  // Wrap AES key
  const wrappedKey = session.wrapKey(
    graphene.MechanismEnum.AES_KEY_WRAP,
    masterKey,
    keyPair.privateKey
  );

  console.log("wrapped:", wrappedKey.toString("base64"));
  console.log(
    "public modulus:" +
      keyPair.publicKey.getAttribute({ modulus: null }).modulus.toString("hex")
  );
  console.log(
    "public exponent:" +
      keyPair.publicKey
        .getAttribute({ publicExponent: null })
        .publicExponent.toString("hex")
  );

  console.log("success");
} catch (e) {
  softHsm.finalize();
  softHsm.close();
  throw e;
}

softHsm.finalize();
softHsm.close();
