const graphene = require("graphene-pk11");
const crypto = require("crypto");

const softHsm = graphene.Module.load("/usr/local/lib/softhsm/libsofthsm2.so");
softHsm.initialize();

const input = {
  signature:
    "a954e86a4adb438c237c7b5f056331e967d46043bd3e9856cc6c4b02d37d65a10831508561fb18f6f4c0e5a65c110585d825f194b04cfb21f39c73fdb1df131c41b399f7471e5537cdbcceb074c7209d0893baf4167e1f3877e5e06548297a77223cbe68e95f62a3acdd35c61938c6dc04723b2985e7d4b4560ebeef81f0b362",
  publicKey: {
    modulus:
      "d73e69d2bb4bf4ffb17c556b58a9d7c7850c6887235c7eff4de921cae9d6e7d53fc4a91b0fb4cadeaf7fa893856f3f1fde54140f40bcdf21d496c0d5fbd4771e072920db6771b37ee27e1c7ce6688713607c936eda20f453c8ecb29c077a0222114a6eae09079aaadd1de2114fbdab503da3c254e2f9411ce68ca855e735111b",
    exponent: "03",
  },
  payload: {
    transactionId: "aaaa",
  },
};

const hash = (payload) =>
  crypto.createHash("sha256").update(JSON.stringify(payload)).digest("hex");

try {
  const slot = softHsm.getSlots(0, true);
  const session = slot.open(
    graphene.SessionFlag.SERIAL_SESSION | graphene.SessionFlag.RW_SESSION
  );
  session.login("1234");

  const publicKey = session.create({
    class: graphene.ObjectClass.PUBLIC_KEY,
    keyType: graphene.KeyType.RSA,
    modulus: Buffer.from(input.publicKey.modulus, "hex"),
    publicExponent: Buffer.from(input.publicKey.exponent, "hex"),
    token: false,
    verify: true,
  });

  const verify = session.createVerify(
    graphene.MechanismEnum.SHA256_RSA_PKCS,
    publicKey
  );
  verify.update(Buffer.from(hash(input.payload), "utf8"));

  const isVerified = verify.final(Buffer.from(input.signature, "hex"));

  console.log("Is verified: " + isVerified);
} catch (e) {
  softHsm.finalize();
  softHsm.close();
  throw e;
}

softHsm.finalize();
softHsm.close();
