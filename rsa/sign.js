const graphene = require("graphene-pk11");
const crypto = require("crypto");

const softHsm = graphene.Module.load("/usr/local/lib/softhsm/libsofthsm2.so");
softHsm.initialize();

const input = {
  wrappedKey:
    "x1aDOwtrpvlJmCGHQNP7FaEpcR5Cse1CK2N2dS2/N6U8jeUWkf9bzuq0udBXr0ew9l91coLcW6Wtk/g0YMkWuv6DC4qaqdaBpUwxkb6L2VLnBE/cmO/+DdC20smsKs5Z0MCkoD7c4edry0T8VbTITyVjuHNP99aZvKFW1y2MpaB5ZN/ZqVOguC+uOQsZAlGBuPujhYdS8jrYGh/yGuG3F0CujGB7KOCZvCPT+6N68R9BCD3fcNoJR6jUAR+sJWOaSh4EBnYWtfipOzYxX9LE0s7yHgrl5Dxg0EwPt2EToTXcKPoDECEpWv6yT4c4JA2hstSuwTLDEQl4y9oC1ImNE7UD6ZvYBrZrgmBXgwSH8LSqKMJES4iRnCxPid9AoyFR9n9HQzVcSdIECyDWRDb6q1K4NqsKSVw3333Vv6w4kZctqLuW4NG0VmvlrzhQsUxM8N5elINHW1/u4ynTCtqvnQPUqbllqbo6c84yBD+UGqfWefVw+/lYjXAMmHaV5jHjfJsC1OI0V4GPYohNGW58aYQstqCecrY8dcFbTzSe789BS+6vknljovPnu/f2eEFmHhUlrFvAyrUJCnBzWABPmwDcxMHiFBs1dVQ5EwzreuhNdeHn7vWMRgW/z31pwOcNTFr2fHwmQfMzWjC57qozAuSEvVIDjPpYJjfXt02A9La6mNYm7/2GZezR11w8zEFuE/AUxCGyJAwtrJjEIRFfbAW+ydWGVOv9nBvXAVKxRWZo7LqImo3Xw1KIlnqrksovhjaTxxl9wykmI49sfJZpuzVVk1MCOjqESMdzwh2RImUWkmn7aPwBc5YdIw41sM/J1e7N37KKLer15ruM3/PSJnEwUeX5Ojg3\n",
  payload: {
    transactionId: "aaaa",
  },
};

const hash = (payload) =>
  crypto.createHash("sha256").update(JSON.stringify(payload)).digest("hex");

try {
  const slot = softHsm.getSlots(0, true);
  const session = slot.open(
    graphene.SessionFlag.SERIAL_SESSION | graphene.SessionFlag.RW_SESSION
  );
  session.login("1234");

  const objects = session.find({
    class: graphene.ObjectClass.SECRET_KEY,
    keyType: graphene.KeyType.AES,
    label: "MasterKey3",
  });
  if (!objects.length) {
    throw new Error("Cannot get master key");
  }
  const masterKey = objects.items(0);

  const wrappedKeyBuffer = Buffer.from(input.wrappedKey, "base64");

  const privKey = session.unwrapKey(
    graphene.MechanismEnum.AES_KEY_WRAP,
    masterKey,
    wrappedKeyBuffer,
    {
      class: graphene.ObjectClass.PRIVATE_KEY,
      keyType: graphene.KeyType.RSA,
      decrypt: true,
      sign: true,
      unwrap: true,
      token: false,
      private: true,
    }
  );

  const sign = session.createSign(
    graphene.MechanismEnum.SHA256_RSA_PKCS,
    privKey
  );

  sign.update(Buffer.from(hash(input.payload), "utf8"));

  const signature = sign.final();

  console.log("Signature: " + signature.toString("hex"));
} catch (e) {
  softHsm.finalize();
  softHsm.close();
  throw e;
}

softHsm.finalize();
softHsm.close();
