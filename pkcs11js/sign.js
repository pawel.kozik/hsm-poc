const pkcs11js = require("pkcs11js");
const pkcs11 = new pkcs11js.PKCS11();

const input = {
  data: "Hello world",
};

pkcs11.load("/opt/homebrew/Cellar/softhsm/2.6.1/lib/softhsm/libsofthsm2.so");

pkcs11.C_Initialize();

try {
  const slotList = pkcs11.C_GetSlotList(true);
  const session = pkcs11.C_OpenSession(
    slotList[0],
    pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION
  );

  try {
    pkcs11.C_Login(session, 1, "1234");

    try {
      const privateKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PRIVATE_KEY },
        { type: pkcs11js.CKA_LABEL, value: "SigningKey" },
      ];

      pkcs11.C_FindObjectsInit(session, privateKeyTemplate);

      const objs = [];
      let obj;
      while ((obj = pkcs11.C_FindObjects(session))) objs.push(obj);
      pkcs11.C_FindObjectsFinal(session);

      if (objs.length === 0) throw new Error("Private key not found");

      const key = objs[0];

      const payload = Buffer.from(input.data, "utf8");
      pkcs11.C_SignInit(
        session,
        { mechanism: pkcs11js.CKM_SHA256_RSA_PKCS },
        key
      );

      pkcs11.C_SignUpdate(session, payload);
      const signature = pkcs11.C_SignFinal(session, Buffer(256));

      console.log("Signature:", signature.toString("base64"));
    } finally {
      pkcs11.C_Logout(session);
    }
  } finally {
    pkcs11.C_CloseSession(session);
  }
} finally {
  pkcs11.C_Finalize();
}
