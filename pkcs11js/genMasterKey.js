const pkcs11js = require("pkcs11js");
const pkcs11 = new pkcs11js.PKCS11();

const input = {
  data: "Hello world",
};

pkcs11.load("/opt/homebrew/Cellar/softhsm/2.6.1/lib/softhsm/libsofthsm2.so");

pkcs11.C_Initialize();

try {
  const slotList = pkcs11.C_GetSlotList(true);
  const session = pkcs11.C_OpenSession(
    slotList[0],
    pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION
  );

  try {
    pkcs11.C_Login(session, 1, "1234");

    try {
      const masterKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
        { type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_AES },
        { type: pkcs11js.CKA_LABEL, value: "MasterKey2" },
        { type: pkcs11js.CKA_WRAP, value: true },
        { type: pkcs11js.CKA_UNWRAP, value: true },
        { type: pkcs11js.CKA_ENCRYPT, value: true },
        { type: pkcs11js.CKA_DECRYPT, value: true },
        { type: pkcs11js.CKA_VALUE_LEN, value: 32 },
        { type: pkcs11js.CKA_TOKEN, value: true }, // store is HSM storage
      ];

      const masterKey = pkcs11.C_GenerateKey(
        session,
        { mechanism: pkcs11js.CKM_AES_KEY_GEN },
        masterKeyTemplate
      );
    } finally {
      pkcs11.C_Logout(session);
    }
  } finally {
    pkcs11.C_CloseSession(session);
  }
} finally {
  pkcs11.C_Finalize();
}
