const pkcs11js = require("pkcs11js");
const pkcs11 = new pkcs11js.PKCS11();

const input = {
  data: "Hello world",
};

pkcs11.load("/opt/homebrew/Cellar/softhsm/2.6.1/lib/softhsm/libsofthsm2.so");

pkcs11.C_Initialize();

try {
  const slotList = pkcs11.C_GetSlotList(true);
  const session = pkcs11.C_OpenSession(
    slotList[0],
    pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION
  );

  try {
    pkcs11.C_Login(session, 1, "s10-token-pin");

    try {
      var publicKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PUBLIC_KEY },
        { type: pkcs11js.CKA_TOKEN, value: false },
        { type: pkcs11js.CKA_LABEL, value: "My RSA Public Key" },
        { type: pkcs11js.CKA_PUBLIC_EXPONENT, value: new Buffer([1, 0, 1]) },
        { type: pkcs11js.CKA_MODULUS_BITS, value: 2048 },
        { type: pkcs11js.CKA_VERIFY, value: true },
      ];
      var privateKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PRIVATE_KEY },
        { type: pkcs11js.CKA_TOKEN, value: false },
        { type: pkcs11js.CKA_LABEL, value: "My RSA Private Key" },
        { type: pkcs11js.CKA_SIGN, value: true },
      ];
      var keys = pkcs11.C_GenerateKeyPair(
        session,
        { mechanism: pkcs11js.CKM_RSA_PKCS_KEY_PAIR_GEN },
        publicKeyTemplate,
        privateKeyTemplate
      );

      console.log(keys.publicKey.toString("base64"));
      throw new Error("aaa");

      // end test
      const secretKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_SECRET_KEY },
        { type: pkcs11js.CKA_LABEL, value: "MasterKey2" },
      ];

      pkcs11.C_FindObjectsInit(session, secretKeyTemplate);

      const objs = [];
      let obj;
      while ((obj = pkcs11.C_FindObjects(session))) objs.push(obj);
      pkcs11.C_FindObjectsFinal(session);

      console.log("Found " + objs.length + " key(s)");

      if (objs.length === 0) throw new Error("Master key not found");

      const masterKeyHandle = objs[0];

      var publicKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PUBLIC_KEY },
        { type: pkcs11js.CKA_TOKEN, value: false },
        //{ type: pkcs11js.CKA_LABEL, value: "My RSA Public Key" },
        //{ type: pkcs11js.CKA_PUBLIC_EXPONENT, value: new Buffer([1, 0, 1]) },
        { type: pkcs11js.CKA_MODULUS_BITS, value: 4096 },
        { type: pkcs11js.CKA_VERIFY, value: true },
        // { type: pkcs11js.CKA_VERIFY, value: true },
      ];
      var privateKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PRIVATE_KEY },
        { type: pkcs11js.CKA_TOKEN, value: false },
        //{ type: pkcs11js.CKA_LABEL, value: "My RSA Private Key" },
        { type: pkcs11js.CKA_SIGN, value: true },
        { type: pkcs11js.CKA_EXTRACTABLE, value: true },
      ];

      //
      // const privateKeyTemplate = [
      //   { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PRIVATE_KEY },
      //   { type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_RSA },
      //   //{ type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_EC },
      //   // { type: pkcs11js.CKA_TOKEN, value: true },
      //   // { type: pkcs11js.CKA_PRIVATE, value: true },
      //   // { type: pkcs11js.CKA_SENSITIVE, value: true },
      //   // { type: pkcs11js.CKA_EXTRACTABLE, value: true },
      //   // { type: pkcs11js.CKA_WRAP, value: true },
      // ];
      // const publicKeyTemplate = [
      //   { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PUBLIC_KEY },
      //   { type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_RSA },
      //   //{ type: pkcs11js.CKA_KEY_TYPE, value: pkcs11js.CKK_EC },
      //   // { type: pkcs11js.CKA_TOKEN, value: true },
      //   // {
      //   //   type: pkcs11js.CKA_EC_PARAMS,
      //   //   value: Buffer.from("06082a8648ce3d030107", "hex"),
      //   // },
      // ];

      const keyPair = pkcs11.C_GenerateKeyPair(
        session,
        { mechanism: pkcs11js.CKM_RSA_PKCS_KEY_PAIR_GEN },
        //{ mechanism: pkcs11js.CKM_EC_KEY_PAIR_GEN },
        publicKeyTemplate,
        privateKeyTemplate
      );

      // let res = Buffer.allocUnsafe(2048);
      //
      // const wrappedKey = pkcs11.C_WrapKey(
      //   session,
      //   { mechanism: pkcs11js.CKM_AES_KEY_WRAP },
      //   masterKeyHandle,
      //   keyPair.privateKey,
      //   res
      // );
      //
      // console.log("Private: " + wrappedKey.toString("base64"));
      console.log(keyPair.privateKey);
      console.log("Public: " + keyPair.publicKey.toString("base64"));

      // const payload = Buffer.from(input.data, "utf8");
      // pkcs11.C_SignInit(
      //   session,
      //   { mechanism: pkcs11js.CKM_SHA256_RSA_PKCS },
      //   key
      // );
      //
      // pkcs11.C_SignUpdate(session, payload);
      // const signature = pkcs11.C_SignFinal(session, Buffer(256));
      //
      // console.log("Signature:", signature.toString("base64"));
    } finally {
      pkcs11.C_Logout(session);
    }
  } finally {
    pkcs11.C_CloseSession(session);
  }
} finally {
  pkcs11.C_Finalize();
}
