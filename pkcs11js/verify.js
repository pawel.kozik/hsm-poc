const pkcs11js = require("pkcs11js");
const pkcs11 = new pkcs11js.PKCS11();

const input = {
  data: "Hello world",
  signature:
    "Eto0aFZsuMinqzw8ebgnmEUMhuXs+6dl3/P0xeSY9v5/w9l1BsV+wQdMdtKygh01aGKoW0FgN32jNK2G8dOJApLNVV60F2pL2kZ2dURl8TfLIgfwekJRCVlOdKs82J2aJxVVfuotf/Ex4t38oaBKx1Be2sStCVn47v+Jeou3RyB3gQurOUZixI5uHdT4H0WXaPZvrKdajsEJZHRB+9prP4xTC7vkrMO2XqqtT6wBXTgqTjhqR++UIDpf5aNCgGCZk+Y212IodZ4rySCEGpwHb5PkyinsznkpJZS91TiWgitq8+5l9L35wg/KpLB3roAVxNa36YFLFijDkc/yqZSKLg==",
};

pkcs11.load("/opt/homebrew/Cellar/softhsm/2.6.1/lib/softhsm/libsofthsm2.so");

pkcs11.C_Initialize();

try {
  const slotList = pkcs11.C_GetSlotList(true);
  const session = pkcs11.C_OpenSession(
    slotList[0],
    pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION
  );

  try {
    pkcs11.C_Login(session, 1, "1234");

    try {
      const privateKeyTemplate = [
        { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PUBLIC_KEY },
        { type: pkcs11js.CKA_LABEL, value: "SigningKey" },
      ];

      pkcs11.C_FindObjectsInit(session, privateKeyTemplate);

      const objs = [];
      let obj;
      while ((obj = pkcs11.C_FindObjects(session))) objs.push(obj);
      pkcs11.C_FindObjectsFinal(session);

      if (objs.length === 0) throw new Error("Public key not found");

      const key = objs[0];

      console.log(key.toString("hex"));

      const payload = Buffer.from(input.data, "utf8");
      pkcs11.C_VerifyInit(
        session,
        { mechanism: pkcs11js.CKM_SHA256_RSA_PKCS },
        key
      );

      pkcs11.C_VerifyUpdate(session, payload);

      const signature = Buffer.from(input.signature, "base64");
      const verify = pkcs11.C_VerifyFinal(session, signature);

      console.log("Verify:", verify);
    } finally {
      pkcs11.C_Logout(session);
    }
  } finally {
    pkcs11.C_CloseSession(session);
  }
} finally {
  pkcs11.C_Finalize();
}
