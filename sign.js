const graphene = require("graphene-pk11");
const crypto = require("crypto");

const softHsm = graphene.Module.load("/usr/local/lib/softhsm/libsofthsm2.so");
softHsm.initialize();

const input = {
  wrappedKey:
    "FIe9xIw6DfYEi1v9tD4UpypFE49OJRUN7HLOxF1alHtzhxXBMS4/Hkn35aE2eudIyV1YNeX26eX2CovIShjj40oLwz6WQlWU",
  payload: {
    transactionId: "aaaa",
  },
};

const hash = (payload) =>
  crypto.createHash("sha256").update(JSON.stringify(payload)).digest("hex");

try {
  const slot = softHsm.getSlots(0, true);
  const session = slot.open(
    graphene.SessionFlag.SERIAL_SESSION | graphene.SessionFlag.RW_SESSION
  );
  session.login("1234");

  const objects = session.find({
    class: graphene.ObjectClass.SECRET_KEY,
    keyType: graphene.KeyType.AES,
    label: "MasterKey3",
  });
  if (!objects.length) {
    throw new Error("Cannot get master key");
  }
  const masterKey = objects.items(0);

  const wrappedKeyBuffer = Buffer.from(input.wrappedKey, "base64");

  const privKey = session.unwrapKey(
    graphene.MechanismEnum.AES_KEY_WRAP,
    masterKey,
    wrappedKeyBuffer,
    {
      class: graphene.ObjectClass.PRIVATE_KEY,
      keyType: graphene.KeyType.ECDSA,
      sign: true,
      unwrap: true,
      token: false,
      sensitive: true,
    }
  );

  console.log("Private key unwrapped", privKey.handle.toString("base64"));

  const sign = session.createSign(graphene.MechanismEnum.ECDSA, privKey);

  const signature = sign.once(Buffer.from(hash(input.payload), "utf8"));

  console.log("Signature: " + signature.toString("hex"));
} catch (e) {
  softHsm.finalize();
  softHsm.close();
  throw e;
}

softHsm.finalize();
softHsm.close();
