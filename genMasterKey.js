const graphene = require("graphene-pk11");

const softHsm = graphene.Module.load("/opt/homebrew/Cellar/softhsm/2.6.1/lib/softhsm/libsofthsm2.so");
softHsm.initialize();

try {
  const slot = softHsm.getSlots(0, true);
  const session = slot.open(
    graphene.SessionFlag.SERIAL_SESSION | graphene.SessionFlag.RW_SESSION
  );
  session.login("s10-token-pin");

  const masterKey = session.generateKey(graphene.KeyGenMechanism.AES, {
    valueLen: 256 >> 3, // 256bits
    encrypt: true,
    decrypt: true,
    extractable: true, // Should be extractable for wrapping
    wrap: true,
    unwrap: true,
    label: "MasterKey3",
    token: true,
  });
} catch (e) {
  softHsm.finalize();
  softHsm.close();
  throw e;
}

softHsm.finalize();
softHsm.close();
